require 'spec_helper'

describe "postgresql is not installed" do
  describe package('postgresql-common') do
    it { should_not be_installed }
  end
  describe service('postgresql') do
    it { should_not be_running }
  end
end

describe "postgresql::client class applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test -e 'include postgresql::client'") do
    its(:exit_status) { should eq 2 }
  end
end

describe "postgresql-client-common is installed" do
  describe package('postgresql-client-common') do
    it { should be_installed }
  end
end

describe "postgresql::server class applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test -e 'include postgresql::server'") do
    its(:exit_status) { should eq 2 }
  end
end

describe "postgresql-common is installed" do
  describe package('postgresql-common') do
    it { should be_installed }
  end
end

describe "postgresql service is started" do
  # allow time for the service to start
  before(:all) do
    sleep 5
  end
  describe service('postgresql') do
    it { should be_enabled }
    it { should be_running }
  end
end

describe "postgresql is responsive" do
  describe command("pg_isready -h localhost -p 5432") do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should include "accepting connections" }
  end
end
